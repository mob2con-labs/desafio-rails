# API de Conversão de Moedas

A tarefa consiste em desenvolver uma aplicação simples usando Ruby on Rails que exibe listas de conversões de moedas e permite adicionar novos itens a essas listas.

### Essa aplicação possui dois tipos de listas:

1. **Lista pública:** Não é necessário qualquer tipo de autenticação para listar as conversões armazenadas em banco, assim como adicionar novos itens à lista pública;

1. **Lista privada:** Exige autenticação (JWT) usando um token enviado através de um cabeçalho (Bearer), que é gerado através de uma chamada para o endpoint `/login`, passando os atributos `email` e `pasword`.


## Conversões

O modelo de Conversão deve ter os seguintes atributos: `currency_from`, `currency_to`, `rate` e `added_at` (conforme tabela abaixo) e deve ser persistido em banco de dados `PostgreSQL`.

| Currency From | Currency To | Rate |
| ------ | ----- | ----- |
| EUR | BRL | 4.453 |
| EUR | USD | 1.120 |


## Requisitos

- A aplicação deve buscar as taxas de conversão usando o serviço [fixer.io](http://fixer.io) (versão gratuíta), com a `API Key` sendo armazenada nas configurações da aplicação.

- O `currency_from` deve ser sempre o mesmo (isso é uma limitação da conta gratuíta do [fixer.io](http://fixer.io)).

- A taxa de conversão (`rate`) deve ser armazenada com 3 dígitos decimais usando arredondamento para baixo.

- Seed deve conter uma conta de usuário, com `e-mail` e `senha` declarados no `README` do repositório.

- Versão estável mais atual do `Ruby` e `Rails`.

- Testes automatizados.

- `README` deve conter todas os detalhes necessários para executar os testes automatizados, e a aplicação em modos `desenvolvimento` e `produção`.


## Diferencial

- Usar `Docker` e `Docker Compose`.
